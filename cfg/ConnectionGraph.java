package cd.cfg;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import cd.Main;
import cd.exceptions.ToDoException;
import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.Cast;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.Var;
import cd.ir.AstRewriteVisitor;
import cd.ir.AstVisitor;
import cd.ir.BasicBlock;
import cd.ir.ControlFlowGraph;
import cd.ir.Symbol.ArrayTypeSymbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.TypeSymbol;
import cd.ir.Symbol.VariableSymbol;
import cd.ir.Symbol.VariableSymbol.Kind;

public class ConnectionGraph {
	
	public int replaceCounter = 0;
	public int newCounter = 0;

	private final EscapeAnalysisVisitor escapeAnalysisVisitor = new EscapeAnalysisVisitor();
	
	boolean changed = false;
	
	ArrayList<Node> nodes = new ArrayList<ConnectionGraph.Node>();
	
	HashMap<MethodSymbol, Method> methods = new HashMap<MethodSymbol, Method>();
	
	HashMap<Ast, Obj> astObjects = new HashMap<Ast, Obj>();
	
	Obj mainObject;
	
	/** Creates Connection Graph, 
	 * Executes reachability algorithm
	 * Replaces NewObject and NewArray statements if possible
	 */
	public ConnectionGraph(List<ClassDecl> astRoots, File fileBase, boolean debugOut)
	{
		ClassSymbol mainClass = null;
		for (ClassDecl cd : astRoots)
		{	
			for (MethodDecl md : cd.methods())
			{
				Method m = new Method(md.sym);
				methods.put(md.sym, m);
			}
			
			if (cd.sym.name.equals("Main"))
			{
				mainClass = cd.sym;
			}
		}
		
		mainObject = new Obj("$MainObject", mainClass, null);
		MethodSymbol ms = findMethod("main",mainClass);
		Method m = methods.get(ms);
		m.thisRef.pointTo(new NodeSet(mainObject));
		
		do
		{
			changed = false;
			
			for (Method mm : methods.values())
			{
				updateMethod(mm);
			}
		}
		while (changed);
		
		for (Method mm : methods.values())
		{
			setMethodEscape(mm);
		}
		
		for (Node n : nodes)
		{
			n.spreadEscape();
		}
		
		if (debugOut)
		{
			for (Method mm : methods.values())
			{
				debugOutput(fileBase, mm);
			}
		}
		replaceNewExpressions();
	}
	

	/** Outputs a .dot file that shows the Connection Graph from the view of "showMethod"
	 */
	public void debugOutput (File base, Method showMethod)
	{
		HashSet<Node> finishedNodes = new HashSet<Node>();
		
		FileWriter fw;
		try {
			fw = new FileWriter(new File(base.getAbsolutePath()+".CG_"+showMethod.sym.owner.name+"_"+showMethod.sym.name+".dot"));
			fw.write("digraph G {\n");
			fw.write("    graph [ rankdir = \"TD\" ];\n");
			
			int subgraphCounter = 0;
			
			debugOutputNode(fw, "    ", mainObject, finishedNodes, showMethod);
			
			for (Method m : methods.values())
			{
				fw.write("    subgraph cluster_"+(subgraphCounter++)+" {\n");
				fw.write("    label = \""+m.sym.owner.name+"."+m.sym.name+"\"\n");
				
				for (Node n : m.locals.values())
				{
					debugOutputNode(fw, "        ", n, finishedNodes,showMethod);
				}
				
				debugOutputNode(fw, "        ", m.returnRef, finishedNodes, showMethod);
				debugOutputNode(fw, "        ", m.thisRef, finishedNodes, showMethod);
				
				for (Obj o : m.newObjects)
				{
					debugOutputNode(fw, "        ", o, finishedNodes, showMethod);
				}
				
				fw.write("    }\n\n");
			}
			
			for (Node n : nodes)
			{
				debugOutputNode(fw, "    ", n, finishedNodes, showMethod);
				debugOutputConnections(fw, "    ", n, showMethod);
			}
			
			fw.write("}");
			fw.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void debugOutputNode(FileWriter fw, String indent, Node n, HashSet<Node> finishedNodes, Method showMethod) throws IOException
	{
		if (!finishedNodes.contains(n))
		{
			fw.write(indent+"N"+n.index+" [\n");
			if (n instanceof Obj)
			{
				fw.write(indent+"shape = \"box\"\n");
				fw.write(indent+"style = \"filled\"\n");
			}
			else
			{
				fw.write(indent+"shape = \"oval\"\n");
			}
			fw.write(indent+"label = \""+ n.getDebugName() +"\"\n");
			
			if (n.escapes.contains(showMethod))
			{
				fw.write(indent+"color = red\n");
			}
			
			fw.write(indent+"];\n");
			finishedNodes.add(n);
			
			if (n instanceof Obj)
			{
				for (Entry<String, Var> no : ((Obj)n).fields.entrySet())
				{
					debugOutputNode(fw, indent+"    ", no.getValue(), finishedNodes, showMethod);
				}
			}
		}
	}
	
	private void debugOutputConnections(FileWriter fw, String indent, Node n, Method showMethod) throws IOException
	{
		if (n instanceof Obj)
		{
			Obj o = (Obj)n;
			for (Node i : o.fields.values())
			{
				fw.write(indent + "N" + n.index + " -> N" + i.index + ";\n");
			}
		}
		else
		{
			Var o = (Var)n;
			for (Node i : o.nodes)
			{
				fw.write(indent + "N" + n.index + " -> N" + i.index + ";\n");
			}
		}
	}
	
	/** updates the Connection graph by visiting all statements of a method
	 */
	private void updateMethod(Method m)
	{
		LinkedList<BasicBlock> todo = new LinkedList<BasicBlock>();
		todo.addLast(m.sym.ast.cfg.start);
		
		HashSet<BasicBlock> visitedBlocks = new HashSet<BasicBlock>();
		
		while (!todo.isEmpty())
		{
			BasicBlock bb = todo.pollFirst();
			if (!visitedBlocks.contains(bb))
			{
				updateBasicBlock(bb, m);
				visitedBlocks.add(bb);
				todo.addAll(bb.successors);
			}
		}
	}
	
	/** Updates Connection graph by visiting all statements in a basic block
	 */
	private void updateBasicBlock(BasicBlock bb, Method m)
	{
		for (Ast ast : bb.instructions)
		{
			escapeAnalysisVisitor.visit(ast, m);
		}
		
		if (bb.condition != null)
		{
			escapeAnalysisVisitor.visit(bb.condition, m);
		}
	}
	
	/** Sets the initial escape state of the Connection Graph for a certain method
	 */
	private void setMethodEscape(Method m)
	{
		m.returnRef.escapes.add(m);
		m.thisRef.escapes.add(m);
		for (VariableSymbol vs : m.sym.parameters)
		{
			if (vs.type.isReferenceType())
			{
				Var v = m.locals.get(vs.name);
				for (Node n : v.nodes)
				{
					n.escapes.add(m);
				}
			}
		}
	}
	
	/** replaces all NewObject and NewArray expressions, that can be stack allocated
	 */
	private void replaceNewExpressions()
	{
		NewReplaceVisitor newReplaceVisitor = new NewReplaceVisitor();
		for (Method m : methods.values())
		{
			for (BasicBlock bb : m.sym.ast.cfg.allBlocks)
			{
				for (int i = 0; i < bb.instructions.size(); i++)
				{
					newReplaceVisitor.visit(bb.instructions.get(i), m);
				}
				if (bb.condition != null)
				{
					newReplaceVisitor.visit(bb.condition, m);
				}
			}
			
		}
	}
	
	/** Visitor to replace NewObject and NewArray expressions
	 */
	class NewReplaceVisitor extends AstRewriteVisitor<Method>
	{
		@Override
		public Ast newObject(NewObject ast, Method arg) {
			Obj o = astObjects.get(ast);
			newCounter++;
			if (!o.escapes.contains(arg))
			{
				replaceCounter++;
				return new Ast.NewStackObject(ast);
			}
			return ast;
		}
		
		@Override
		public Ast newArray(NewArray ast, Method arg) {
			Obj o = astObjects.get(ast);
			newCounter++;
			if (!o.escapes.contains(arg))
			{
				replaceCounter++;
				return new Ast.NewStackArray(ast);
			}
			return ast;
		}
	}
	
	/** Visitor that constructs the Connection Graph
	 */
	class EscapeAnalysisVisitor extends AstVisitor<NodeSet, Method>
	{
		@Override
		public NodeSet assign(Assign ast, Method arg) {
			if (ast.left().type.isReferenceType())
			{
				NodeSet rightNodes = visit(ast.right(), arg);
				if (rightNodes != null)
				{
					NodeSet leftNodes = visit(ast.left(), arg);
					assert (leftNodes.noObj());
					
					leftNodes.VarsPointTo(rightNodes);
				}
			}
			return null;
		}
		
		@Override
		public NodeSet var(cd.ir.Ast.Var ast, Method arg) {
			if (ast.type.isReferenceType())
			{
				if (ast.sym.kind == Kind.FIELD)
				{
					return arg.thisRef.getFields(ast.sym.name);
				}
				else
				{
					if (!ast.sym.name.equals("__UNINIT__"))
						return new NodeSet(arg.locals.get(ast.sym.name));
					else
						return new NodeSet();
				}
			}
			return null;
		}
		
		@Override
		public NodeSet cast(Cast ast, Method arg) {
			return visit(ast.arg(), arg);
		}
		
		@Override
		public NodeSet field(Field ast, Method arg) {
			if (ast.type.isReferenceType())
			{
				NodeSet n = visit(ast.arg(),arg);
				return n.getFields(ast.sym.name);
			}
			return null;
		}
		
		@Override
		public NodeSet index(Index ast, Method arg) {
			visit(ast.right(),arg);
			NodeSet n = visit(ast.left(), arg);
			return n.getFields("_DATA_");
		}
		
		@Override
		public NodeSet newObject(NewObject ast, Method arg) {
			Obj o = astObjects.get(ast);
			if (o == null)
			{
				o = new Obj("$NewObj", ast.type, arg);
				astObjects.put(ast,  o);
			}
			return new NodeSet(o);
		}
		
		@Override
		public NodeSet newArray(NewArray ast, Method arg) {
			Obj o = astObjects.get(ast);
			if (o == null)
			{
				o = new Obj("$NewArray", ast.type, arg);
				astObjects.put(ast,  o);
			}
			return new NodeSet(o);
		}
		
		@Override
		public NodeSet methodCall(MethodCall ast, Method arg) {
			ArrayList<NodeSet> args = new ArrayList<ConnectionGraph.NodeSet>();
			for (Expr expr : ast.argumentsWithoutReceiver())
			{
				args.add(visit(expr, arg));
			}
			
			NodeSet nodes = visit(ast.receiver(), arg);
			NodeSet receivers = nodes.getObjects();
			
			for (Node n : receivers)
			{
				MethodSymbol m = findMethod(ast.methodName, (ClassSymbol)((Obj)n).type);
				Method invk = methods.get(m);
				methodInvok((Obj)n, args, invk);
			}
			return null;
		}
		
		@Override
		public NodeSet methodCall(MethodCallExpr ast, Method arg) {
			ArrayList<NodeSet> args = new ArrayList<ConnectionGraph.NodeSet>();
			for (Expr expr : ast.argumentsWithoutReceiver())
			{
				args.add(visit(expr, arg));
			}
			NodeSet receivers = visit(ast.receiver(), arg).getObjects();
			
			NodeSet results = new NodeSet();
			for (Node n : receivers)
			{
				MethodSymbol m = findMethod(ast.methodName, (ClassSymbol)((Obj)n).type);
				Method invk = methods.get(m);
				results.addAll(methodInvok((Obj)n, args, invk));
			}
			return results;
		}

		
		private NodeSet methodInvok(Obj thisObjs, ArrayList<NodeSet> args, Method method)
		{
			method.thisRef.pointTo(new NodeSet(thisObjs));
			
			for (int i = 0; i < args.size(); i++)
			{
				NodeSet arg = args.get(i);
				if (arg != null)
				{
					VariableSymbol vs = method.sym.parameters.get(i);
					Var v = method.locals.get(vs.name);
					v.pointTo(arg);
				}
			}
			
			return new NodeSet(method.returnRef);
		}
		
		@Override
		public NodeSet nullConst(NullConst ast, Method arg) {
			return new NodeSet();
		}
		
		@Override
		public NodeSet returnStmt(ReturnStmt ast, Method arg) {
			if (ast.arg() != null)
			{
				if (ast.arg().type.isReferenceType())
				{
					arg.returnRef.pointTo(visit(ast.arg(), arg));
				}
			}
			return null;
		}
		
		@Override
		public NodeSet thisRef(ThisRef ast, Method arg) {
			return new NodeSet(arg.thisRef);
		}
	}
	
	/** Helper Method to find the MethodSymbol of the method that would be invoked for the ClassSymbold of the dynamic type
	 */
	private MethodSymbol findMethod(String name, ClassSymbol c)
	{
		MethodSymbol m = null;
		
		do
		{
			m = c.methods.get(name);
			c = c.superClass;
		}
		while (m == null && c != null);
		
		return m;
	}
	
	/** Stores all Nodes of the Connection Graph belonging to a certain method
	 */
	class Method
	{
		MethodSymbol sym;
		
		Var returnRef;
		Var thisRef;
		HashMap<String, Var> locals = new HashMap<String, ConnectionGraph.Var>();
		
		ArrayList<Obj> newObjects = new ArrayList<ConnectionGraph.Obj>();
		
		public Method(MethodSymbol method)
		{
			returnRef = new Var("returnRef");
			thisRef = new Var("thisRef");
			
			sym = method;
			for (Entry<String, VariableSymbol> vs : method.locals.entrySet())
			{
				if (vs.getValue().type.isReferenceType())
				{
					locals.put(vs.getKey(), new Var(vs.getKey()));
				}
			}
			
			for (VariableSymbol vs : method.parameters)
			{
				if (vs.type.isReferenceType())
				{
					locals.put(vs.name, new Var(vs.name));
				}
			}
		}
	}
	
	/** Connection Graph Node
	 */
	abstract class Node
	{
		HashSet<Method> escapes = new HashSet<Method>();
		String name;
		int index;
		
		Node()
		{
			index = nodes.size();
			nodes.add(this);
			changed = true;
		}
		
		public abstract void spreadEscape();
		
		public String getDebugName()
		{
			return name;
		}
	}
	
	/** Connection Graph Object Node
	 */
	class Obj extends Node
	{
		TypeSymbol type = null;
		HashMap<String, Var> fields = new HashMap<String, ConnectionGraph.Var>();
		
		public Obj(String name, TypeSymbol type, Method m)
		{
			super();
			if (m != null)
				m.newObjects.add(this);
			
			this.name = name;
			changed = true;
			this.type = type;
		}
		
		public void spreadEscape()
		{
			for (Var v : fields.values())
			{
				int numEsc = v.escapes.size();
				v.escapes.addAll(escapes);
				if (numEsc != v.escapes.size())
				{
					changed = true;
					v.spreadEscape();
				}
			}
		}
		
		public void setField(String name, NodeSet n)
		{
			Var v = fields.get(name);
			if (v == null)
			{
				v = new Var(name);
				fields.put(name, v);
			}
			v.pointTo(n);
		}
		
		public Var getField(String name)
		{
			Var v = fields.get(name);
			if (v == null)
			{
				v = new Var(name);
				fields.put(name, v);
			}
			return v;
		}
	}
	
	/** Connection Graph Reference Node
	 */
	class Var extends Node
	{
		NodeSet nodes = new NodeSet();
		
		public Var(String name)
		{
			super();
			this.name = name;
		}
		
		public void spreadEscape()
		{
			for (Node v : nodes)
			{
				int numEsc = v.escapes.size();
				v.escapes.addAll(escapes);
				if (numEsc != v.escapes.size())
				{
					changed = true;
					v.spreadEscape();
				}
			}
		}
		
		public NodeSet getObjects()
		{
			NodeSet newSet = new NodeSet();
			
			for (Node n : nodes)
			{
				if (n instanceof Obj)
				{
					newSet.add((Obj)n);
				}
				else
				{
					newSet.addAll(((Var)n).getObjects());
				}
			}
			
//			if (newSet.isEmpty())
//			{
//				Obj o = new Obj("$Obj");
//				o.escapes = true;
//				nodes.add(o);
//				newSet.add(o);
//			}
			return newSet;
		}
		
		public void pointTo(NodeSet n)
		{
			int s = nodes.size();
			nodes.addAll(n);
			if (s != nodes.size())
			{
				changed = true;
			}
		}
		
		public void setFields(String name, NodeSet n)
		{
			NodeSet s = getFields(name);
			for (Node v : s)
			{
				((Var)v).pointTo(n);
			}
		}
		
		public NodeSet getFields(String name)
		{
			NodeSet newSet = new NodeSet();
			
			NodeSet os = getObjects();
			for (Node o : os)
			{
				newSet.add(((Obj)o).getField(name));
			}
			return newSet;
		}
	}
	
	/** Set of Nodes with some additional functionality
	 */
	class NodeSet extends HashSet<Node>
	{
		private static final long serialVersionUID = 7949647658084727814L;

		public NodeSet(Node o)
		{
			this.add(o);
		}
		
		public NodeSet()
		{
			
		}
		
		public boolean noObj()
		{
			for (Node n : this)
			{
				if (n instanceof Obj)
				{
					return false;
				}
			}
			return true;
		}
		
		public boolean noVar()
		{
			for (Node n : this)
			{
				if (n instanceof Var)
				{
					return false;
				}
			}
			return true;
		}
		
		public void VarsPointTo(NodeSet n)
		{
			for (Node i : this)
			{
				if (i instanceof Var)
				{
					((Var)i).pointTo(n);
				}
			}
		}
		
		public NodeSet getFields(String name)
		{
			NodeSet newSet = new NodeSet();
			for (Node n : this)
			{
				if (n instanceof Var)
				{
					newSet.addAll(((Var)n).getFields(name));
				}
			}
			return newSet;
		}
		
		public NodeSet getObjects()
		{
			NodeSet newSet = new NodeSet();
			for (Node n : this)
			{
				if (n instanceof Var)
				{
					newSet.addAll(((Var)n).getObjects());
				}
				else
				{
					newSet.add(n);
				}
			}
			return newSet;
		}
	}
}
